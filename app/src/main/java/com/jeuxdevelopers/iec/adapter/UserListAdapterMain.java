package com.jeuxdevelopers.iec.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.jeuxdevelopers.iec.R;
import com.jeuxdevelopers.iec.activity.ChatActivity;
import com.jeuxdevelopers.iec.helper.Constants;
import com.jeuxdevelopers.iec.models.UserModels;

import java.util.ArrayList;
import java.util.List;

public class UserListAdapterMain extends RecyclerView.Adapter<UserListAdapterMain.UserViewHolder> implements Filterable {
    private Context mContext;
    private ArrayList<UserModels> mArrayListModels;
    private ArrayList<UserModels> userListFiltered;


    public UserListAdapterMain(Context context) {
        mContext = context;
        mArrayListModels = new ArrayList<>();
        userListFiltered = new ArrayList<>();
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext)
                .inflate(R.layout.item_view_users, parent, false);
        return new UserViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {
        UserModels models = mArrayListModels.get(position);
        Glide.with(mContext).load(models.getImage()).into(holder.mImageView);
        holder.mTextView.setText(models.getName());
    }

    @Override
    public int getItemCount() {
        return userListFiltered.size();
    }

    public void setData(ArrayList<UserModels> models) {
        mArrayListModels = models;
        userListFiltered = models;
        notifyDataSetChanged();
    }

    public class UserViewHolder extends RecyclerView.ViewHolder {
        public ImageView mImageView;
        public TextView mTextView;

        public UserViewHolder(@NonNull View itemView) {
            super(itemView);
            mImageView = itemView.findViewById(R.id.civ_Picture);
            mTextView = itemView.findViewById(R.id.tv_Name);
            mImageView.setOnClickListener(v -> {
                Intent intent = new Intent(mContext, ChatActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(ChatActivity.KEY_ACTIVITY_TYPE, Constants.SINGLE_CHAT_ACTIVITY_TYPE);
                intent.putExtra(ChatActivity.KEY_USER_MODEL, mArrayListModels.get(getAdapterPosition()));
                mContext.startActivity(intent);
            });

        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    userListFiltered = mArrayListModels;
                } else {
                    ArrayList<UserModels> filteredList = new ArrayList<>();
                    for (UserModels row : mArrayListModels) {
                        Log.v("onQueryText", "CharString: " + charString);
                        Log.v("onQueryText", "CharSequence: " + charString);
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getName().toLowerCase().contains(charString.toLowerCase())) {
                            //Log.v("onQueryText", "title: : " + row.getTitlte());
                            filteredList.add(row);
                        }
                    }
                    userListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = userListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                Log.v("onQueryText", "publishResult: " + "CharSeq: " + charSequence + "    Filter Result: " + filterResults);
                userListFiltered = (ArrayList<UserModels>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
