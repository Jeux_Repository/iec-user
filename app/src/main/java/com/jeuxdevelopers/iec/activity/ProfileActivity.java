package com.jeuxdevelopers.iec.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.google.android.material.appbar.AppBarLayout;
import com.jeuxdevelopers.iec.R;
import com.jeuxdevelopers.iec.sessions.IecSessions;
import com.mikhaellopez.circularimageview.CircularImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.iv_Back)
    ImageView ivBack;
    @BindView(R.id.mainHeading)
    RelativeLayout mainHeading;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.appBarLayout)
    AppBarLayout appBarLayout;
    @BindView(R.id.imgProfile)
    CircularImageView imgProfile;
    @BindView(R.id.txtView_name)
    TextView txtViewName;
    @BindView(R.id.txtView_email)
    TextView txtViewEmail;
    @BindView(R.id.btn_logout)
    Button btnLogout;
    private IecSessions mSessions
            ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        init();
        settingListener();
    }

    private void settingListener() {
        ivBack.setOnClickListener(this);
        btnLogout.setOnClickListener(this);
    }

    private void init() {
        mSessions = new IecSessions(this);
        Glide.with(this).load(mSessions.getImageLink()).into(imgProfile);
        txtViewEmail.setText(mSessions.getEmail());
        txtViewName.setText(mSessions.getDisplayName());
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == ivBack.getId()) {
            finish();
        } else if (id == btnLogout.getId()) {
            mSessions.setLogin("", "", "", "", false);
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }
}
