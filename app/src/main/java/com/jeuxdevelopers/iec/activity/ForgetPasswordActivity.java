package com.jeuxdevelopers.iec.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.JsonObject;
import com.jeuxdevelopers.iec.R;
import com.jeuxdevelopers.iec.helper.HelperClass;
import com.jeuxdevelopers.iec.networks.APiClient;
import com.jeuxdevelopers.iec.networks.APiInterface;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgetPasswordActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.editTxt_email_f)
    EditText editTxtEmailF;
    @BindView(R.id.btn_register_f)
    Button btnRegisterF;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        ButterKnife.bind(this);
        settingListener();
    }

    private void settingListener() {
        btnRegisterF.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id= v.getId();
        if (!editTxtEmailF.getText().toString().equals("")) {
            resetPassword();
        } else {
            HelperClass.createToast(this,"Please enter email address");
        }
    }

    private void resetPassword() {
        String email = editTxtEmailF.getText().toString();
        APiInterface aPiInterface = APiClient.createRetrofitService();
        Call<JSONObject> userCall = aPiInterface.resetpassword(email,APiClient.API_KEY);
        userCall.enqueue(new Callback<JSONObject>() {
            @Override
            public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                if (response.body()!=null) {
                    HelperClass.createToast(ForgetPasswordActivity.this,"Instruction for reset password has been send on your email ");
                } else {
                    HelperClass.createToast(ForgetPasswordActivity.this,"Some thing went wrong");
                }
            }

            @Override
            public void onFailure(Call<JSONObject> call, Throwable t) {
                HelperClass.createToast(ForgetPasswordActivity.this,t.getMessage());
            }
        });

    }
}
