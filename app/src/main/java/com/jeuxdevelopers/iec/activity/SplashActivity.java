package com.jeuxdevelopers.iec.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.jeuxdevelopers.iec.R;
import com.jeuxdevelopers.iec.sessions.IecSessions;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashActivity extends AppCompatActivity {

    @BindView(R.id.splash_love)
    ImageView splashLove;
    private IecSessions mSessions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        init();
        startAnimation();
        handlerThread();
    }

    private void handlerThread() {
        new Handler().postDelayed(() -> {

            if (mSessions.isLogin()) {
                startActivity(new Intent(SplashActivity.this,MainActivity.class));
            } else {
                startActivity(new Intent(SplashActivity.this,LoginActivity.class));
            }
            finish();
        }, 3500);
    }

    private void startAnimation() {
        Animation myAnim = AnimationUtils.loadAnimation(this, R.anim.mytransition);
        splashLove.startAnimation(myAnim);
    }

    private void init() {
        mSessions = new IecSessions(this);
    }
}
