package com.jeuxdevelopers.iec.activity;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.solver.widgets.Helper;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.jeuxdevelopers.iec.R;
import com.jeuxdevelopers.iec.adapter.CreateGroupAdapter;
import com.jeuxdevelopers.iec.helper.HelperClass;
import com.jeuxdevelopers.iec.models.UserModels;
import com.jeuxdevelopers.iec.networks.APiClient;
import com.jeuxdevelopers.iec.networks.APiInterface;
import com.jeuxdevelopers.iec.sessions.IecSessions;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateGroupActivity extends AppCompatActivity implements View.OnClickListener, CreateGroupAdapter.CheckBoxListener {

    private static final int RESULT_LOAD_IMAGE = 1245;
    @BindView(R.id.imgViee_back)
    ImageView imgVieeBack;
    @BindView(R.id.txx)
    TextView txx;
    @BindView(R.id.head_new_group)
    RelativeLayout headNewGroup;
    @BindView(R.id.txx2)
    TextView txx2;
    @BindView(R.id.imgView_group)
    CircularImageView imgViewGroup;
    @BindView(R.id.imgView_add)
    CircularImageView imgViewAdd;
    @BindView(R.id.img_layout)
    RelativeLayout imgLayout;
    @BindView(R.id.et_GroupName)
    EditText etGroupName;
    @BindView(R.id.recyclerview_users)
    RecyclerView recyclerviewUsers;
    @BindView(R.id.btn_CreateGroup)
    Button btnCreateGroup;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    private ArrayList<UserModels> mArrayListModels = new ArrayList<>();
    private static String TAG = "CreateGroupActivity__";
    public static String KEY_USER_LIST = "key_list";
    private CreateGroupAdapter mAdapter;
    private Uri mGroupImageUri;
    private JsonObject mJsonObject;
    private IecSessions mSessions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group);
        ButterKnife.bind(this);
        init();
        gettingIntent();
        settingRecyclerView();
        settingListener();


    }
    private void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    private void settingListener() {
        imgViewGroup.setOnClickListener(this);
        mAdapter.setListener(this);
        btnCreateGroup.setOnClickListener(this);
        imgVieeBack.setOnClickListener(this);
    }

    private void settingRecyclerView() {
        mAdapter = new CreateGroupAdapter(this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerviewUsers.setLayoutManager(mLayoutManager);
        recyclerviewUsers.setItemAnimator(new DefaultItemAnimator());
        recyclerviewUsers.setAdapter(mAdapter);
        mAdapter.setData(mArrayListModels);
    }

    private void gettingIntent() {
        mArrayListModels = (ArrayList<UserModels>) getIntent().getSerializableExtra(KEY_USER_LIST);
    }

    private void init() {
        mJsonObject = new JsonObject();
        mSessions = new IecSessions(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == imgViewGroup.getId()) {
            selectImageFromGallery();
        } else if (id == btnCreateGroup.getId()) {
            if (applyValidation()) {
                createGroup();
            }

        } else if (id == imgVieeBack.getId()) {
            finish();
        }
    }

    private boolean applyValidation() {
        if (etGroupName.getText().toString().equals("")) {
            HelperClass.createToast(this,"Please enter group name");
            return false;
        } else if (mGroupImageUri == null) {
            HelperClass.createToast(this,"Please enter group name");
            return false;
        } else {
            return true;
        }
    }

    public void selectImageFromGallery() {
        Intent i = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(i, RESULT_LOAD_IMAGE);
    }


    private void setGroupImage(Uri selectedImage) {
        mGroupImageUri = selectedImage;
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(selectedImage,
                filePathColumn, null, null, null);
        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String picturePath = cursor.getString(columnIndex);
        cursor.close();
        imgViewGroup.setImageBitmap(BitmapFactory.decodeFile(picturePath));
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            setGroupImage(selectedImage);


        }
    }

    public void createGroup() {
        showProgressBar();
        mJsonObject.addProperty(mSessions.getUSerId(), "admin");
        String name = etGroupName.getText().toString();
        File file = new File(HelperClass.getPath(mGroupImageUri, this));
        Gson gson = new Gson();
        String data = gson.toJson(mJsonObject);
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        builder.addFormDataPart("displayName", name);
        builder.addFormDataPart("users", data);
        builder.addFormDataPart("Image", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
        MultipartBody body = builder.build();
        APiInterface aPiInterface = APiClient.createRetrofitService();
        Call<JsonObject> userCall = aPiInterface.createGroup(body, APiClient.API_KEY);
        userCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                hideProgressBar();
                if (response.body() != null && response.message().equals("OK")) {
                    Log.i(TAG, "Group Created");
                    JsonObject jsonObject = response.body();
                    String id = jsonObject.get("id").getAsString();
                    addGroupIdToUserServer(id);
                    HelperClass.createToast(CreateGroupActivity.this,"GroupCreated Successfully");
                    Intent intent = new Intent(CreateGroupActivity.this,MainActivity.class);
                    startActivity(intent);

                } else {
                    Log.i(TAG, "" + response.message());
                    hideProgressBar();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.i(TAG, "" + t.getMessage());
                hideProgressBar();
            }
        });

    }

    private void addGroupIdToUserServer(String groupId) {
        Set<String> temp = mJsonObject.keySet();
        ArrayList<String> arrayList = new ArrayList();
        arrayList.addAll(temp);
        for (String key : arrayList) {
            uploadGroupId(key, groupId);;
        }


    }

    private void uploadGroupId(String userId, String groupId) {
        String id = UUID.randomUUID().toString();
        Map<String, String> arrayList = new HashMap<>();
        arrayList.put("Groups[" + id + "]", groupId);
        APiInterface aPiInterface = APiClient.createRetrofitService();
        Call<JsonObject> userCall = aPiInterface.saveGroupId(userId, groupId, APiClient.API_KEY);
        userCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Log.i(TAG, "SaveGroupResponse : " + response.message());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.i(TAG, "Save Group Failure : " + t.getMessage());
            }
        });

    }

    @Override
    public void onCheckedChanged(int position, boolean isChecked) {
        UserModels models = mArrayListModels.get(position);
        if (isChecked) {
            mJsonObject.addProperty(models.getUserId(), "user");
        } else {
            mJsonObject.remove(models.getUserId());
        }
    }
}
