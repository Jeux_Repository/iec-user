package com.jeuxdevelopers.iec.models;

public class ChatModel {
    private String mMessageId;
    private String mMessage, mNameOFReciever, mTime,mSenderId;
    private String mGroupId;

    public String getGroupId() {
        return mGroupId;
    }

    public void setGroupId(String groupId) {
        mGroupId = groupId;
    }

    public String getSenderId() {
        return mSenderId;
    }

    public String getMessageId() {
        return mMessageId;
    }

    public void setMessageId(String messageId) {
        mMessageId = messageId;
    }

    public void setSenderId(String senderId) {
        mSenderId = senderId;
    }

    public ChatModel(String message, String nameOFReciever, String time) {
        mMessage = message;
        mNameOFReciever = nameOFReciever;
        mTime = time;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getNameOFReciever() {
        return mNameOFReciever;
    }

    public void setNameOFReciever(String nameOFReciever) {
        mNameOFReciever = nameOFReciever;
    }

    public String getTime() {
        return mTime;
    }

    public void setTime(String time) {
        mTime = time;
    }
}
