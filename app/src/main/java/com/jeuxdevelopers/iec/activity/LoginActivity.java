package com.jeuxdevelopers.iec.activity;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.JsonObject;
import com.jeuxdevelopers.iec.R;
import com.jeuxdevelopers.iec.helper.HelperClass;
import com.jeuxdevelopers.iec.networks.APiClient;
import com.jeuxdevelopers.iec.networks.APiInterface;
import com.jeuxdevelopers.iec.sessions.IecSessions;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.et_email_login)
    EditText etEmailLogin;
    @BindView(R.id.et_password_login)
    EditText etPasswordLogin;
    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.tv_forget_pass)
    TextView tvForgetPass;
    @BindView(R.id.btn_create_account)
    Button btnCreateAccount;
    @BindView(R.id.progressBarLogin)
    ProgressBar progressBarLogin;
    private String mEmail, mPassword;
    private static String TAG = "LoginActivity__";
    private IecSessions mSessions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        init();
        settingListener();
        //checkPermissions();
    }

    private void checkPermissions() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA
                ).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {/* ... */}

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                token.continuePermissionRequest();
            }
        }).check();
    }


    private void settingListener() {
        btnLogin.setOnClickListener(this);
        btnCreateAccount.setOnClickListener(this);
        tvForgetPass.setOnClickListener(this);
    }

    private boolean applyValidation() {
        mEmail = etEmailLogin.getText().toString();
        mPassword = etPasswordLogin.getText().toString();
        if (!isValidEmail(mEmail)) {
            etEmailLogin.setError("Email is not valid");
            return false;
        } else if (mPassword.isEmpty()) {
            etPasswordLogin.setError("password cannot be empty");
            return false;
        } else {
            return true;
        }
    }

    private void init() {
        mSessions = new IecSessions(this);

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == btnLogin.getId()) {
            if (applyValidation()) {
                createLoginRequests();
            }
        } else if (id == btnCreateAccount.getId()) {
            navigateToSignUpActivity();
        } else if (id == tvForgetPass.getId()) {
            startActivity(new Intent(this, ForgetPasswordActivity.class));
        }
    }

    private void navigateToSignUpActivity() {
        Intent intent = new Intent(this, SignupActivity.class);
        startActivity(intent);
    }

    private void createLoginRequests() {
        showProgressBar();
        APiInterface aPiInterface = APiClient.createRetrofitService();
        Call<JsonObject> userCall = aPiInterface.loginRequest(mEmail, mPassword, APiClient.API_KEY);
        userCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                hideProgressBar();
                if (response.body() != null && response.errorBody() == null) {
                    if (response.code() == 200 || response.code() == 201 || response.message().equals("OK")) {
                        JsonObject jsonObject = response.body();
                        String email = jsonObject.get("email").getAsString();
                        boolean emailVarified = jsonObject.get("emailVerified").getAsBoolean();
                        String uId = jsonObject.get("uid").getAsString();
                        if (!emailVarified) {
                            HelperClass.createToast(LoginActivity.this, "Your email address is not verified please verify it first");

                        } else {
                            getTheUSerData(uId);

                        }
                        Log.i(TAG, "" + response.message());
                        Log.i(TAG, "" + response.code());

                    }
                } else if (response.errorBody() != null) {

                    try {
                        JSONObject jsonObject = new JSONObject(response.errorBody().string());
                        JSONObject message = jsonObject.getJSONObject("content");
                        Log.i("asffs", "Afsa");
                        HelperClass.createToast(getApplicationContext(), "incorrect email or password");

                    } catch (IOException e) {
                        e.printStackTrace();
                        HelperClass.createToast(getApplicationContext(), "Incorrect email or password correct ");
                    } catch (JSONException e) {
                        e.printStackTrace();
                        HelperClass.createToast(getApplicationContext(), "incorrect email or password");
                    }
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                hideProgressBar();
                HelperClass.createToast(getApplicationContext(), "" + t.getMessage());
            }
        });
    }

    private void getTheUSerData(String uId) {
        APiInterface aPiInterface = APiClient.createRetrofitService();
        Call<JsonObject> userCall = aPiInterface.getSingleUSerData(uId, APiClient.API_KEY);
        userCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                hideProgressBar();
                if (response.body() != null && response.errorBody() == null) {
                    if (response.code() == 200 || response.code() == 201 || response.message().equals("OK")) {
                        HelperClass.createToast(LoginActivity.this, "Login Successfull");
                        JsonObject jsonObject = response.body();
                        String email = jsonObject.get("email").getAsString();
                        String displayName = jsonObject.get("displayName").getAsString();
                        String image = jsonObject.get("Image").getAsString();
                        mSessions.setLogin(uId, email, displayName, image, true);
                        navigateToMainActivity();
                    }
                } else if (response.errorBody() != null) {
                    HelperClass.createToast(LoginActivity.this, "" + response.message());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                HelperClass.createToast(LoginActivity.this, "" + t.getMessage());
                hideProgressBar();
            }
        });
    }

    private void navigateToMainActivity() {
        startActivity(new Intent(this, MainActivity.class));
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    private void showProgressBar() {
        progressBarLogin.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar() {
        progressBarLogin.setVisibility(View.GONE);
    }
}
