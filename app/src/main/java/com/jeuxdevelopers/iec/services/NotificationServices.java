package com.jeuxdevelopers.iec.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.google.gson.JsonObject;
import com.jeuxdevelopers.iec.R;
import com.jeuxdevelopers.iec.activity.ChatActivity;
import com.jeuxdevelopers.iec.helper.Constants;
import com.jeuxdevelopers.iec.models.UserModels;
import com.jeuxdevelopers.iec.networks.APiClient;
import com.jeuxdevelopers.iec.networks.APiInterface;
import com.jeuxdevelopers.iec.sessions.IecSessions;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationServices extends Service {
    private IecSessions mSessions;
    private static String TAG = "NotificationServices___";
    private ArrayList<UserModels> mArrayListModesl = new ArrayList<>();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        Log.i(TAG, "Service start");
        super.onCreate();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            startMyOwnForeground();
        else
            startForeground(1, new Notification());


    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mSessions = new IecSessions(this);
        getUSerNotifications();
        Log.i(TAG, "on Start Command Called");
        return START_STICKY;
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private void startMyOwnForeground() {
        String NOTIFICATION_CHANNEL_ID = "com.example.simpleapp";
        String channelName = "My Background Service";
        NotificationChannel chan = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_NONE);
        chan.setLightColor(Color.BLUE);
        chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert manager != null;
        manager.createNotificationChannel(chan);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        Notification notification = notificationBuilder.setOngoing(true)
                .setSmallIcon(R.drawable.ic_logo)
                .setContentTitle("App is running in background")
                .setPriority(NotificationManager.IMPORTANCE_MIN)
                .setCategory(Notification.CATEGORY_SERVICE)
                .build();
        startForeground(2, notification);
    }

    public void getUSerNotifications() {
        APiInterface aPiInterface = APiClient.createRetrofitService();
        Call<JsonObject> userCall = aPiInterface.getUserNotifications(mSessions.getUSerId(), APiClient.API_KEY);
        userCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                try {
                    if (response.body() != null && response.body().size() != 0) {
                        ArrayList<UserModels> arrayList = new ArrayList<>();
                        JsonObject jsonObject = response.body();
                        Set<String> rootId = jsonObject.keySet();
                        Set<String> notificatioId = new HashSet<>();
                        JsonObject jsonObject1 = null;
                        for (String s : rootId) {
                            jsonObject1 = jsonObject.get(s).getAsJsonObject();

                        }
                        notificatioId = jsonObject1.keySet();
                        for (String s : notificatioId) {
                            JsonObject jsonObject2 = jsonObject1.get(s).getAsJsonObject();
                            if (jsonObject2.size() != 0) {
                                String messsage = jsonObject2.get("message").getAsString();
                                String nameOfReciever = jsonObject2.get("nameOfReciver").getAsString();
                                String senderID = jsonObject2.get("senderId").getAsString();
                                String senderName = jsonObject2.get("senderName").getAsString();
                                String time = jsonObject2.get("time").getAsString();
                                String senderImage = jsonObject2.get("userImage").getAsString();

                                UserModels models = new UserModels();
                                models.setMessage(messsage);
                                models.setNotificationid(s);
                                models.setNameOfReciever(nameOfReciever);
                                models.setUserId(senderID);
                                models.setName(senderName);
                                models.setTime(time);
                                models.setImage(senderImage);
                                mArrayListModesl.add(models);
                            }
                        }

                        for (int i = 0; i < mArrayListModesl.size(); i++) {
                            UserModels model = new UserModels();
                            model.setImage(mArrayListModesl.get(i).getImage());
                            model.setUserId(mArrayListModesl.get(i).getUserId());
                            model.setName(mArrayListModesl.get(i).getName());
                            sendNotification(mArrayListModesl.get(i).getName(), mArrayListModesl.get(i).getMessage(), model);
                            if (i == mArrayListModesl.size() - 1) {
                                deleteNotification();
                            }
                        }
                    } else {
                        getUSerNotifications();
                    }
                } catch (Exception e) {

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.i(TAG, "" + t.getMessage());
                getUSerNotifications();
            }
        });
    }

    private void deleteNotification() {
        APiInterface aPiInterface = APiClient.createRetrofitService();
        Call<JsonObject> call = aPiInterface.deleteNotification(mSessions.getUSerId(), APiClient.API_KEY);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Log.i(TAG, "" + response.message());
                getUSerNotifications();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.i(TAG, "" + t.getMessage());
                getUSerNotifications();
            }
        });

    }

    private void sendNotification(String title, String message, UserModels model) {
        final int min = 20;
        final int max = 80;
        final int random = new Random().nextInt((max - min) + 1) + min;
        Uri notification_sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra("key_activity_type", Constants.SINGLE_CHAT_ACTIVITY_TYPE);
        intent.putExtra("key_model", model);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "com.iec")
                .setSmallIcon(R.drawable.ic_logo)
                .setContentTitle(title)
                .setContentText(message)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
        builder.setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "IEC";
            String description = message;
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("com.iec", name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build();
            channel.setSound(notification_sound, audioAttributes);
            //   NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            builder.setSound(notification_sound);
        }

        Random r = new Random();
        int i1 = r.nextInt(80 - 65) + 65;
        notificationManager.cancelAll();
// notificationId is a unique int for each notification that you must define
        notificationManager.notify(i1, builder.build());
    }
}
