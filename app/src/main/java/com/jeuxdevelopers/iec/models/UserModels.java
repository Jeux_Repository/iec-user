package com.jeuxdevelopers.iec.models;

import java.io.Serializable;

public class UserModels implements Serializable {
    private String userId,mImage,mName,mEmail;
    private String mMessage,mNameOfReciever,mTime;
    private String mSenderId,mNotificationid;

    public String getSenderId() {
        return mSenderId;
    }

    public void setSenderId(String senderId) {
        mSenderId = senderId;
    }

    public String getNotificationid() {
        return mNotificationid;
    }

    public void setNotificationid(String notificationid) {
        mNotificationid = notificationid;
    }

    public String getTime() {
        return mTime;
    }

    public void setTime(String time) {
        mTime = time;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getNameOfReciever() {
        return mNameOfReciever;
    }

    public void setNameOfReciever(String nameOfReciever) {
        mNameOfReciever = nameOfReciever;
    }

    public UserModels() {
    }

    public UserModels(String userId, String image, String name, String email) {
        this.userId = userId;
        mImage = image;
        mName = name;
        mEmail = email;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String image) {
        mImage = image;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }
}
