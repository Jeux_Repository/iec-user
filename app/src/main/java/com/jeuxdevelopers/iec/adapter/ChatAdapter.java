package com.jeuxdevelopers.iec.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

import com.bumptech.glide.Glide;
import com.jeuxdevelopers.iec.R;
import com.jeuxdevelopers.iec.activity.ImageViewActivity;
import com.jeuxdevelopers.iec.models.ChatModel;
import com.jeuxdevelopers.iec.models.UserModels;
import com.jeuxdevelopers.iec.sessions.IecSessions;

import java.util.ArrayList;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ChatViewHolder> {
    private Context mContext;
    private ArrayList<ChatModel> mArrayListModels = new ArrayList<>();
    private IecSessions mSessions;


    public ChatAdapter( Context context) {
        mContext = context;
        mSessions = new IecSessions(mContext);

    }

    @NonNull
    @Override
    public ChatViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext)
                .inflate(R.layout.itemview_chat_row, parent, false);
        return new ChatViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ChatViewHolder holder, int position) {
        ChatModel model = mArrayListModels.get(position);
        holder.mTextView.setVisibility(View.VISIBLE);
        if ( Patterns.WEB_URL.matcher(model.getMessage()).matches()) {
            Glide.with(mContext).load(model.getMessage()).into(holder.mImageView);
            holder.mImageView.setVisibility(View.VISIBLE);
            holder.mTextView.setVisibility(View.GONE);
        } else {
            holder.mTextView.setText(model.getMessage());
            holder.mTextView.setVisibility(View.VISIBLE);
            holder.mImageView.setVisibility(View.GONE);
        }

        if (model.getSenderId().equals(mSessions.getUSerId())) {
            holder.mLinearLayoutBgChat.setBackgroundResource(R.drawable.bg_chat_right);
            holder.mLinearLayout.setGravity(Gravity.END);
            holder.mTextView.setTextColor(ContextCompat.getColor(mContext,R.color.colorPrimary));
        } else {
            holder.mLinearLayoutBgChat.setBackgroundResource(R.drawable.bg_chat_left);
            holder.mLinearLayout.setGravity(Gravity.START);
            holder.mTextView.setTextColor(ContextCompat.getColor(mContext,R.color.colorBlack));
        }


    }

    @Override
    public int getItemCount() {
        return mArrayListModels.size();
    }

    public void setData(ArrayList<ChatModel> list) {
        mArrayListModels = list;
        notifyDataSetChanged();

    }


    public class ChatViewHolder extends ViewHolder {
        ImageView mImageView;
         TextView mTextView;
        LinearLayout mLinearLayout,mLinearLayoutBgChat;
        public ChatViewHolder(@NonNull View itemView) {
            super(itemView);
              mImageView = itemView.findViewById(R.id.image_id_message_row);
            mTextView = itemView.findViewById(R.id.Message_View_Id);
            mLinearLayout = itemView.findViewById(R.id.Message_Layout_Control);
            mLinearLayoutBgChat = itemView.findViewById(R.id.message_row_message_bg_card);
            mImageView.setOnClickListener(v -> {
                Intent intent = new Intent(mContext, ImageViewActivity.class);
                intent.putExtra("IMAGE_LINK",mArrayListModels.get(getAdapterPosition()).getMessage());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            });
        }
    }
}
