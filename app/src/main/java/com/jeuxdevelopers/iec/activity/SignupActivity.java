package com.jeuxdevelopers.iec.activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.JsonObject;
import com.jeuxdevelopers.iec.R;
import com.jeuxdevelopers.iec.helper.HelperClass;
import com.jeuxdevelopers.iec.networks.APiClient;
import com.jeuxdevelopers.iec.networks.APiInterface;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignupActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int RESULT_LOAD_IMAGE = 1312;
    @BindView(R.id.img_profile_pic)
    CircularImageView imgProfilePic;
    @BindView(R.id.et_register_name)
    EditText etRegisterName;
    @BindView(R.id.et_register_email)
    EditText etRegisterEmail;
    @BindView(R.id.et_register_password)
    EditText etRegisterPassword;
    @BindView(R.id.btn_signup)
    Button btnSignup;
    @BindView(R.id.btn_already)
    Button btnAlready;
    @BindView(R.id.progressBar_signUp)
    ProgressBar progressBarSignUp;
    private String mEmail, mName, mPassword;
    private static String TAG = "SignupActivity__";
    private Uri mProfilePicUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);
        settingListener();
    }

    private void settingListener() {
        btnSignup.setOnClickListener(this);
        imgProfilePic.setOnClickListener(this);
        btnAlready.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == btnSignup.getId()) {
            signupUser();
        } else if (id == imgProfilePic.getId()) {
            checkPermission();
        }  else if (id == btnAlready.getId()) {
            startActivity(new Intent(this,LoginActivity.class));
        }
    }

    private void signupUser() {
        if (applyValidation()) {
            uploadDataToServer();
        }
    }

    private void uploadDataToServer() {
        showProgressBar();
        APiInterface aPiInterface = APiClient.createRetrofitService();
        Call<JsonObject> userCall = aPiInterface.createInitialSignup(mEmail, mPassword, APiClient.API_KEY);
        userCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Log.i(TAG, "" + response.message());
                if (response.body() != null && (response.errorBody()==null) && response.message().equals("OK")) {
                    String id = response.body().get("uid").getAsString();
                    String email = response.body().get("email").getAsString();
                    sendVerficationEmail(id, email);
                    Log.i(TAG, "id__" + id);
                    Log.i(TAG, "email__" + email);

                }  else if (response.errorBody() != null) {
                    HelperClass.createLongToast(SignupActivity.this,"Email is already registered or invalid email");
                    hideProgressBar();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.i(TAG, "Upload data : " + t.getMessage());
                HelperClass.createLongToast(SignupActivity.this,""+t.getMessage());
                hideProgressBar();
            }
        });
    }

    private void sendVerficationEmail(String id, String email) {

        APiInterface aPiInterface = APiClient.createRetrofitService();
        Call<JsonObject> userCall = aPiInterface.sendVerificationEmail(id, email, APiClient.API_KEY);
        userCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                try {
                    if (response.body() != null && (response.errorBody() == null) && response.message().equals("OK")) {
                        Log.i(TAG, "VErification mail send");
                        saveUserName(id);
                    } else if (response.errorBody() != null) {
                        HelperClass.createLongToast(SignupActivity.this, "Email is already registered or invalid email");
                        hideProgressBar();
                    }
                } catch (Exception e) {

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                hideProgressBar();
                HelperClass.createLongToast(SignupActivity.this,""+t.getMessage());
                Log.i(TAG, "Verification email : " + t.getMessage());
            }
        });

    }

    private void saveUserName(String id) {
        File file = new File(HelperClass.getPath(mProfilePicUri,this));
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        builder.addFormDataPart("displayName", mName);
        builder.addFormDataPart("email", mEmail);
        builder.addFormDataPart("Image", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
        MultipartBody body = builder.build();
        APiInterface aPiInterface = APiClient.createRetrofitService();
        Call<JsonObject> userCall = aPiInterface.saveUserData(id, body, APiClient.API_KEY);
        userCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                try {
                    if (response.body() != null && (response.errorBody() == null) && response.message().equals("OK")) {
                        createAlertDialog();
                        hideProgressBar();
                    } else if (response.errorBody() != null) {
                        HelperClass.createLongToast(SignupActivity.this, "" + response.message());
                        hideProgressBar();
                    }
                }catch (Exception e) {

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.i(TAG, "Save User Name" + t.getMessage());
                HelperClass.createLongToast(SignupActivity.this,""+t.getMessage());
                hideProgressBar();

            }
        });
    }

    private boolean applyValidation() {
        mEmail = etRegisterEmail.getText().toString();
        mName = etRegisterName.getText().toString();
        mPassword = etRegisterPassword.getText().toString();
        if (!isValidEmail(mEmail)) {
            etRegisterEmail.setError("Email is not valid");
            return false;
        } else if (mName.isEmpty()) {
            etRegisterName.setError("Please enter name");
            return false;
        } else if (mPassword.isEmpty()) {
            etRegisterPassword.setError("Please provide the password");
            return false;
        } else if (mPassword.length() < 6) {
            etRegisterPassword.setError("Minimum password length should be six");
            return false;
        } else if (mProfilePicUri == null){
            HelperClass.createToast(this,"Please Select Profile pic first");
            return false;
        } else {
            return true;
        }
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public void selectImageFromGallery() {
        Intent i = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(i, RESULT_LOAD_IMAGE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            setProfilePic(selectedImage);


        }
    }

    private void setProfilePic(Uri selectedImage) {
        mProfilePicUri = selectedImage;
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(selectedImage,
                filePathColumn, null, null, null);
        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String picturePath = cursor.getString(columnIndex);
        cursor.close();
        imgProfilePic.setImageBitmap(BitmapFactory.decodeFile(picturePath));
    }

    public void showProgressBar() {
        progressBarSignUp.setVisibility(View.VISIBLE);
    }

    public void hideProgressBar() {
        progressBarSignUp.setVisibility(View.GONE);
    }


    public void createAlertDialog() {

        new AlertDialog.Builder(this)
                .setTitle("Email Verification")
                .setCancelable(true)
                .setMessage("Verification email has been successfully send to your email account please verify it to login")
                .setPositiveButton(android.R.string.yes, (dialog, which) -> startActivity(new Intent(SignupActivity.this,LoginActivity.class))).show();
    }

    public void checkPermission() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                ).withListener(new MultiplePermissionsListener() {
            @Override public void onPermissionsChecked(MultiplePermissionsReport report) {
                if (report.areAllPermissionsGranted()) {
                    selectImageFromGallery();
                    Log.i(TAG,"Permission granted");
                 } else if (report.isAnyPermissionPermanentlyDenied()) {
                    HelperClass.createToast(SignupActivity.this,"Please grant permissions manually in the settings");
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                    intent.setData(uri);
                    startActivity(intent);
                }
                else {
                    HelperClass.createToast(SignupActivity.this,"Storage Permissions needed to select profile pic");
                    Log.i(TAG,"Permission denied");
                }
            }
            @Override public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {token.continuePermissionRequest();}

        }).check();
    }
}
