package com.jeuxdevelopers.iec.models;

import java.io.Serializable;

public class GroupInfoModel implements Serializable {
    private String mImage,mName,mUSersString;
    private String mGroupId;

    public String getGroupId() {
        return mGroupId;
    }

    public void setGroupId(String groupId) {
        mGroupId = groupId;
    }

    public GroupInfoModel(String image, String name, String USersString) {
        mImage = image;
        mName = name;
        mUSersString = USersString;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String image) {
        mImage = image;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getUSersString() {
        return mUSersString;
    }

    public void setUSersString(String USersString) {
        mUSersString = USersString;
    }
}
