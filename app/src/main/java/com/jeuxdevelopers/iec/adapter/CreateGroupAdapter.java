package com.jeuxdevelopers.iec.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.jeuxdevelopers.iec.R;
import com.jeuxdevelopers.iec.models.UserModels;

import java.util.ArrayList;

public class CreateGroupAdapter extends RecyclerView.Adapter<CreateGroupAdapter.GroupViewHolder> {
    private Context mContext;
    private ArrayList<UserModels> mArrayListModels = new ArrayList<>();
    private CheckBoxListener mListener;

    public CreateGroupAdapter(Context context) {
        mContext = context;
    }



    @NonNull
    @Override
    public GroupViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.itemview_create_group_adapter, parent, false);
        return new GroupViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GroupViewHolder holder, int position) {
        UserModels models = mArrayListModels.get(position);
        Glide.with(mContext).load(models.getImage()).into(holder.mImageView);
        holder.mTextViewName.setText(models.getName());


    }

    @Override
    public int getItemCount() {
        return mArrayListModels.size();
    }

    public void setData(ArrayList<UserModels> arrayListModels) {
        mArrayListModels = arrayListModels;
        notifyDataSetChanged();
    }

    class GroupViewHolder extends RecyclerView.ViewHolder {
        CheckBox mCheckBox;
        ImageView mImageView;
        TextView mTextViewName;

        GroupViewHolder(@NonNull View itemView) {
            super(itemView);
            mCheckBox = itemView.findViewById(R.id.chkBox);
            mImageView = itemView.findViewById(R.id.imgeView_user);
            mTextViewName = itemView.findViewById(R.id.textView_name);
            mCheckBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            mListener.onCheckedChanged(getAdapterPosition(),isChecked);
            });
        }
    }


    public interface CheckBoxListener {
        public void onCheckedChanged(int position,boolean isChecked);
    }

    public void setListener(CheckBoxListener listener) {
        mListener = listener;
    }
}

