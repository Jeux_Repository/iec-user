package com.jeuxdevelopers.iec.fragment;


import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.jeuxdevelopers.iec.R;
import com.jeuxdevelopers.iec.adapter.InboxAdapter;
import com.jeuxdevelopers.iec.models.UserModels;
import com.jeuxdevelopers.iec.sessions.IecSessions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

public class InboxFragment extends Fragment implements View.OnClickListener {

    private View fragmentView;
    private Context context;
    private ImageView iv_BackInbox;
    private SearchView searchView_Inbox;
    private RecyclerView rclView_UsersInbox;
    private ProgressBar progressBar_Inbox;
    private InboxAdapter inboxAdapter;
    public static ArrayList<UserModels> inboxUsersList = new ArrayList<>();
    private IecSessions preferences;
    private String TAG = "INBOX_DATA";
    private boolean isFirstTime = true;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentView = inflater.inflate(R.layout.fragment_inbox, container, false);
        context = getActivity();
        linkViews();
        setClickListeners();
        setupRecyclerView();
        return fragmentView;
    }

    @Override
    public void onResume() {
        super.onResume();
        init();
    }

    private void init() {
        preferences = new IecSessions(context);
        getInboxData();
    }

    private void setupRecyclerView() {
        inboxAdapter = new InboxAdapter(context);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(context);
        rclView_UsersInbox.setLayoutManager(mLayoutManager);
        rclView_UsersInbox.setItemAnimator(new DefaultItemAnimator());
        rclView_UsersInbox.setHasFixedSize(true);
        rclView_UsersInbox.setAdapter(inboxAdapter);
    }

    private void linkViews() {
        iv_BackInbox = fragmentView.findViewById(R.id.iv_BackInbox);
        searchView_Inbox = fragmentView.findViewById(R.id.searchView_Inbox);
        rclView_UsersInbox = fragmentView.findViewById(R.id.rclView_UsersInbox);
        progressBar_Inbox = fragmentView.findViewById(R.id.progressBar_Inbox);
    }

    private void setClickListeners() {
        iv_BackInbox.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_BackInbox:
                getActivity().onBackPressed();
                break;
        }
    }

    private void getInboxData() {

        if (isFirstTime) {
            showProgress();
        }

        Log.v(TAG, "API: " + APIs.USERS + APIs.API_KEY);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                APIs.USERS + APIs.API_KEY, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.v("GetALlUsers", "Response: " + response.toString());
                        if (response.length() > 0) {
                            Iterator<String> iterator = response.keys();
                            while (iterator.hasNext()) {
                                String id = iterator.next();
                                if (!id.equals(preferences.getUserId())) {
                                    try {
                                        JSONObject object = response.getJSONObject(id);
                                        String email = object.getString("email");
                                        String name = object.getString("displayName");
                                        String imageLink = object.getString("Image");
                                        UsersModel model = new UsersModel(id, imageLink, name, email);
                                        if (object.has("latestMessage") && object.has("time")) {
                                            String latestMessage = object.getString("latestMessage");
                                            String time = object.getString("time");
                                            model.setLastMessage(latestMessage);
                                            model.setTimeInMills(Long.parseLong(time));
                                            inboxUsersList.add(model);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                            Collections.sort(inboxUsersList, new SortByCreationDate());
                            Log.v(TAG, "UserList Size " + inboxUsersList.size());
                            if (inboxUsersList.size() != 0) {
                                inboxAdapter.setData(inboxUsersList);
                            }
                        }

                        hideProgress();
                        isFirstTime = false;
                        getInboxData();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Log.v("GetALlUsers", "Error: " + error.getMessage());
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjReq, TAG);
    }

    class SortByCreationDate implements Comparator<UserModels> {
        public int compare(UserModels a, UserModels b) {
            return Long.compare(a.getTimeInMills(), b.getTimeInMills());
        }
    }

    private void showProgress() {
        progressBar_Inbox.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        progressBar_Inbox.setVisibility(View.GONE);
    }
}
