package com.jeuxdevelopers.iec.networks;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APiClient {
    public static String BASE_URL = "http://iec.api.avenirdevelopers.com/";
    private static Retrofit sRetrofit = null;
    public static String API_KEY = "nJ7kOc203yi02eW5px11";
    static final OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .readTimeout(50, TimeUnit.SECONDS)
            .connectTimeout(50, TimeUnit.SECONDS)
            .build();

    public static APiInterface createRetrofitService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL).client(new OkHttpClient())
                .addConverterFactory(GsonConverterFactory.create()).client(okHttpClient)
                .build();
        return retrofit.create(APiInterface.class);
    }
}
