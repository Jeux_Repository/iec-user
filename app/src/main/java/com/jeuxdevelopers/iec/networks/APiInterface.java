package com.jeuxdevelopers.iec.networks;

import com.google.gson.JsonObject;

import org.json.JSONObject;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APiInterface {
    @FormUrlEncoded
    @POST("api/users/register")
    Call<JsonObject> createInitialSignup(@Field("email") String email, @Field("password") String password, @Query("api_key") String key);
    @FormUrlEncoded
    @POST("api/users/sendActivation")
    Call<JsonObject> sendVerificationEmail(@Field("uid") String id, @Field("email") String email, @Query("api_key") String key);
    @POST("api/users/{user_id}")
    Call<JsonObject> saveUserData(@Path(value = "user_id",encoded = true) String userId, @Body RequestBody file, @Query("api_key") String key);
    @FormUrlEncoded
    @POST("api/users/login")
    Call<JsonObject> loginRequest(@Field("email") String email, @Field("password") String password,  @Query("api_key") String key);
    @GET("api/users/{user_id}")
    Call<JsonObject> getSingleUSerData(@Path(value = "user_id",encoded = true) String userId, @Query("api_key") String key);
    @GET("api/users")
    Call<JsonObject> getAllUsers(@Query("api_key") String apiKey);
    @POST("api/chats/{id}/{id1}")
    Call<JsonObject> sendSingleMessage(@Path(value = "id",encoded = true) String senderId,@Path(value = "id1",encoded = true) String receiverId,@Query("api_key") String key,@Body RequestBody file);
    @GET("api/chats/{id}/{id1}")
    Call<JsonObject> getChatForSingleUser(@Path(value = "id",encoded = true) String senderId,@Path(value = "id1",encoded = true) String receiverId,@Query("api_key") String key);
    @POST("api/groups")
    Call<JsonObject> createGroup(@Body RequestBody file,@Query("api_key") String key);
    @GET("api/users/{user_id}/groups/{group_id}")
    Call<JsonObject> saveGroupId(@Path(value = "user_id",encoded = true) String userId,@Path(value = "group_id",encoded = true) String groupId,@Query("api_key") String key);
    @GET("api/groups/{id}")
    Call<JsonObject> getGroupDetail(@Path(value = "id",encoded = true) String userId,@Query("api_key") String key);
    @POST("api/groups/{id}/messages")
    Call<JsonObject> sendSingleGroupMessage(@Path(value = "id", encoded = true) String groupId, @Body RequestBody file, @Query("api_key") String key);
    @GET("api/groups/{id}/messages")
    Call<JsonObject> getAllGroupsChat(@Path(value = "id", encoded = true) String groupId,@Query("api_key") String key);
    @POST("api/notifications/{id}/{id1}")
    Call<JsonObject> sendNotification(@Path(value = "id", encoded = true) String senderId, @Path(value = "id1", encoded = true) String receiverId, @Body RequestBody file,@Query("api_key") String key);
    @GET("api/notifications/{id}")
    Call<JsonObject> getUserNotifications(@Path(value = "id", encoded = true) String userId,@Query("api_key") String key);
    @FormUrlEncoded
    @POST("api/users/resetPassword")
    Call<JSONObject> resetpassword(@Field("email") String email,@Query("api_key") String key);
    /*@DELETE("api/notifications/{id}/{id1}/{id2}")*/
    /*Call<JsonObject> deleteNotification(@Path(value = "id", encoded = true) String recieverId,@Path(value = "id1", encoded = true) String senderId,@Path(value = "id2", encoded = true) String notificationId,@Query("api_key") String key);*/
    @DELETE("api/notifications/{id}")
    Call<JsonObject> deleteNotification(@Path(value = "id", encoded = true) String recieverId,@Query("api_key") String key);


}
