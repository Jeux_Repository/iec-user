package com.jeuxdevelopers.iec.activity;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.appbar.AppBarLayout;
import com.google.gson.JsonObject;
import com.jeuxdevelopers.iec.R;
import com.jeuxdevelopers.iec.adapter.ChatAdapter;
import com.jeuxdevelopers.iec.helper.Constants;
import com.jeuxdevelopers.iec.helper.HelperClass;
import com.jeuxdevelopers.iec.models.ChatModel;
import com.jeuxdevelopers.iec.models.GroupInfoModel;
import com.jeuxdevelopers.iec.models.UserModels;
import com.jeuxdevelopers.iec.networks.APiClient;
import com.jeuxdevelopers.iec.networks.APiInterface;
import com.jeuxdevelopers.iec.sessions.IecSessions;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int RESULT_LOAD_IMAGE = 1212;
    @BindView(R.id.iv_Back)
    ImageView ivBack;
    @BindView(R.id.friend_image_in_chat)
    CircularImageView friendImageInChat;
    @BindView(R.id.Friend_Name_View_ID)
    TextView FriendNameViewID;
    @BindView(R.id.info_user_btn)
    ImageView infoUserBtn;
    @BindView(R.id.mainHeading)
    RelativeLayout mainHeading;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.appBarLayout)
    AppBarLayout appBarLayout;


    @BindView(R.id.recView_chat)
    RecyclerView recViewChat;
    @BindView(R.id.editText_message)
    EditText editTextMessage;
    @BindView(R.id.imgView_thumb_file)
    ImageView imgViewThumbFile;
    @BindView(R.id.ll1chat)
    LinearLayout ll1chat;
    @BindView(R.id.Send_Data)
    ImageView SendData;
    @BindView(R.id.linearLayout_send)
    LinearLayout linearLayoutSend;
    @BindView(R.id.rl_Bottom)
    RelativeLayout rlBottom;
    @BindView(R.id.root_chat)
    RelativeLayout rootChat;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private ChatAdapter mChatAdapter;
    private UserModels mUserModel;
    private IecSessions mSessions;
    private ArrayList<ChatModel> mArrayListChatsModel = new ArrayList<>();
    public static String KEY_USER_MODEL = "key_model";
    private static String TAG = "ChatActivity___";
    private Handler mHandler;
    private Uri mImageUri;
    public static String KEY_ACTIVITY_TYPE = "key_activity_type";
    private String mActivityType;
    private GroupInfoModel mGroupInfoModel;
    private Runnable r;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);
        init();
        gettingIntent();
        settingRecyclerView();
        settingListener();
        if (mActivityType.equals(Constants.SINGLE_CHAT_ACTIVITY_TYPE)) {
            handlerRequestsForGettingChats();
        } else if (mActivityType.equals(Constants.GROUPACTIVITY_Type)) {
            getAllChatGroups();
        }

    }

    private void handlerRequestsForGettingChats() {
        try {
            mHandler = new Handler();
            r = new Runnable() {
                public void run() {
                    mHandler.postDelayed(this, 1000);
                    getSingleUSerChats();
                }
            };

            mHandler.postDelayed(r, 1000);
        } catch (Exception e) {
            mHandler.removeCallbacks(null);
        }

    }
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }



    private void getSingleUSerChats() {
        if (mArrayListChatsModel.size()==0) {
            showProgressBar();
        }
        APiInterface aPiInterface = APiClient.createRetrofitService();
        Call<JsonObject> userCall = aPiInterface.getChatForSingleUser(mSessions.getUSerId(), mUserModel.getUserId(), APiClient.API_KEY);
        userCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                try {
                    hideProgressBar();
                    ArrayList<ChatModel> arrayList = new ArrayList<>();
                    if (response.body() != null && response.message().equals("OK")) {
                        JsonObject jsonObject = response.body();
                        Set<String> set = jsonObject.keySet();
                        ArrayList<String> list = new ArrayList<String>(set);
                        for (int i = 0; i < jsonObject.size(); i++) {
                            String id = list.get(i);

                            JsonObject jsonObject1 = jsonObject.get(id).getAsJsonObject();
                            if (jsonObject != null && jsonObject.size() != 0) {
                                String message = jsonObject1.get("message").getAsString();
                                String name = jsonObject1.get("nameOfReciver").getAsString();
                                String time = jsonObject1.get("time").getAsString();
                                String senderId = jsonObject1.get("senderId").getAsString();
                                ChatModel models = new ChatModel(message, name, time);
                                models.setSenderId(senderId);
                                models.setMessageId(id);
                                arrayList.add(models);
                            }

                        }
                        if (mArrayListChatsModel.size() == 0 && arrayList.size() != 0) {
                            mArrayListChatsModel = arrayList;
                            mChatAdapter.setData(mArrayListChatsModel);
                            recViewChat.scrollToPosition(mChatAdapter.getItemCount() - 1);
                            Log.i(TAG, "ArrayList init" + mArrayListChatsModel.size());
                        } else if (arrayList.size() > mArrayListChatsModel.size()) {
                            Log.i(TAG, "Beefore" + mArrayListChatsModel.size());
                            int insertIndex = mArrayListChatsModel.size();
                            for (int i = mArrayListChatsModel.size(); i < arrayList.size(); i++) {
                                {
                                    if (!mArrayListChatsModel.get(i - 1).getTime().equals(arrayList.get(i).getTime())) {
                                        mArrayListChatsModel.add(arrayList.get(i));
                                        mChatAdapter.notifyItemRangeInserted(insertIndex, mArrayListChatsModel.size());
                                        recViewChat.scrollToPosition(mChatAdapter.getItemCount() - 1);
                                    }
                                }
                            }

                            Log.i(TAG, "After" + mArrayListChatsModel.size());
                        }

                    }
                } catch (Exception e) {

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.i(TAG, "" + t.getMessage());
                hideProgressBar();

            }
        });


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mHandler != null && r != null) {
            mHandler.removeCallbacks(r);
        }
    }

    private void gettingIntent() {
        mActivityType = getIntent().getStringExtra(KEY_ACTIVITY_TYPE);
        if (mActivityType.equals(Constants.SINGLE_CHAT_ACTIVITY_TYPE)) {
            mUserModel = (UserModels) getIntent().getSerializableExtra(KEY_USER_MODEL);
            Glide.with(this).load(mUserModel.getImage()).into(friendImageInChat);
            FriendNameViewID.setText(mUserModel.getName());
        } else if (mActivityType.equals(Constants.GROUPACTIVITY_Type)) {
            mGroupInfoModel = (GroupInfoModel) getIntent().getSerializableExtra(KEY_USER_MODEL);
            Glide.with(this).load(mGroupInfoModel.getImage()).into(friendImageInChat);
            FriendNameViewID.setText(mGroupInfoModel.getName());
        }

    }

    private void settingListener() {
        linearLayoutSend.setOnClickListener(this);
        SendData.setOnClickListener(this);
        imgViewThumbFile.setOnClickListener(this);
        ivBack.setOnClickListener(this);


    }

    private void init() {
        mSessions = new IecSessions(this);
        setSupportActionBar(toolbar);


    }

    private void settingRecyclerView() {

        mChatAdapter = new ChatAdapter(this);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mLayoutManager.setStackFromEnd(true);
        recViewChat.setLayoutManager(mLayoutManager);
        recViewChat.setItemAnimator(new DefaultItemAnimator());
        recViewChat.setHasFixedSize(true);
        recViewChat.setAdapter(mChatAdapter);


    }

    public void sendChatToServer(String type) {
        Log.i("aasdfaf", "Clicked");
        String message = editTextMessage.getText().toString();
        if (message.equals("")) {
            message = " ";
        }
        editTextMessage.setText("");
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);

        builder.addFormDataPart("nameOfReciver", mUserModel.getName());
        builder.addFormDataPart("time", String.valueOf(HelperClass.getTime()));
        builder.addFormDataPart("senderId", mSessions.getUSerId());
        if (type.equals("image")) {
            File file = new File(HelperClass.getPath(mImageUri, this));
            File comressFile = HelperClass.imageCompression(this, file);
            builder.addFormDataPart("message", file.getName(), RequestBody.create(MediaType.parse("image/*"), comressFile));
        } else if (type.equals("text")) {
            builder.addFormDataPart("message", message);
        }
        MultipartBody body = builder.build();
        APiInterface aPiInterface = APiClient.createRetrofitService();
        Call<JsonObject> userCall = aPiInterface.sendSingleMessage(mSessions.getUSerId(), mUserModel.getUserId(), APiClient.API_KEY, body);
        userCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                ArrayList<ChatModel> arrayList = new ArrayList<>();
                if (response.body() != null) {
                    JsonObject jsonObject = response.body();
                    String message = jsonObject.get("message").getAsString();
                    String name = jsonObject.get("nameOfReciver").getAsString();
                    String time = jsonObject.get("time").getAsString();
                    String senderId = jsonObject.get("senderId").getAsString();
                    ChatModel models = new ChatModel(message, name, time);
                    models.setSenderId(senderId);
                    if (mArrayListChatsModel.size() == 0) {
                        mArrayListChatsModel.add(models);
                        mChatAdapter.setData(mArrayListChatsModel);
                        sendNotification(models);
                    } else {
                        if (!mArrayListChatsModel.get(mArrayListChatsModel.size() - 1).getTime().equals(models.getTime()))
                            mArrayListChatsModel.add(models);
                        //mChatAdapter.notifyItemInserted(mArrayListChatsModel.size());
                        mChatAdapter.notifyDataSetChanged();
                        recViewChat.scrollToPosition(mChatAdapter.getItemCount() - 1);
                        sendNotification(models);
                    }

                }


            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.i(TAG, "" + t.getMessage());
                HelperClass.createToast(ChatActivity.this, "Failed to send message");
            }
        });


    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == SendData.getId()) {
            if (!editTextMessage.getText().toString().equals("")) {
                if (mActivityType.equals(Constants.SINGLE_CHAT_ACTIVITY_TYPE)) {
                    sendChatToServer("text");
                } else if (mActivityType.equals(Constants.GROUPACTIVITY_Type)) {
                    sendSingleGroupMessage("text");
                }
            }
        } else if (id == imgViewThumbFile.getId()) {
            checkPermission();
        } else if (id == ivBack.getId()) {
            finish();
        }
    }
    public void checkPermission() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                ).withListener(new MultiplePermissionsListener() {
            @Override public void onPermissionsChecked(MultiplePermissionsReport report) {
                if (report.areAllPermissionsGranted()) {
                    selectImageFromGallery();
                    Log.i(TAG,"Permission granted");
                } else if (report.isAnyPermissionPermanentlyDenied()) {
                    HelperClass.createToast(ChatActivity.this,"Please grant permissions manually in the settings");
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                    intent.setData(uri);
                    startActivity(intent);
                }
                else {
                    HelperClass.createToast(ChatActivity.this,"Storage Permissions needed to select profile pic");
                    Log.i(TAG,"Permission denied");
                }
            }
            @Override public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {token.continuePermissionRequest();}

        }).check();
    }

    public void selectImageFromGallery() {
        Intent i = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(i, RESULT_LOAD_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            if (mActivityType.equals(Constants.SINGLE_CHAT_ACTIVITY_TYPE)) {
                Uri selectedImage = data.getData();
                mImageUri = selectedImage;
                sendChatToServer("image");
            } else if (mActivityType.equals(Constants.GROUPACTIVITY_Type)) {
                Uri selectedImage = data.getData();
                mImageUri = selectedImage;
                sendSingleGroupMessage("image");
            }


        }
    }

    public void sendSingleGroupMessage(String type) {
        String message = editTextMessage.getText().toString();
        editTextMessage.setText("");
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);

        builder.addFormDataPart("nameOfReciver", mGroupInfoModel.getName());
        builder.addFormDataPart("time", String.valueOf(HelperClass.getTime()));
        builder.addFormDataPart("senderId", mSessions.getUSerId());
        if (type.equals("image")) {
            File file = new File(HelperClass.getPath(mImageUri, this));
            builder.addFormDataPart("message", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
        } else if (type.equals("text")) {
            builder.addFormDataPart("message", message);
        }
        MultipartBody body = builder.build();
        APiInterface aPiInterface = APiClient.createRetrofitService();
        Call<JsonObject> userCall = aPiInterface.sendSingleGroupMessage(mGroupInfoModel.getGroupId(), body, APiClient.API_KEY);
        userCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() != null) {
                    JsonObject jsonObject = response.body();
                    String message = jsonObject.get("message").getAsString();
                    String name = jsonObject.get("nameOfReciver").getAsString();
                    String time = jsonObject.get("time").getAsString();
                    String senderId = jsonObject.get("senderId").getAsString();
                    ChatModel models = new ChatModel(message, name, time);
                    models.setSenderId(senderId);
                    if (mArrayListChatsModel.size() == 0) {
                        mArrayListChatsModel.add(models);
                        mChatAdapter.setData(mArrayListChatsModel);
                        //sendNotification(models);
                    } else {
                        if (!mArrayListChatsModel.get(mArrayListChatsModel.size() - 1).getTime().equals(models.getTime()))
                            mArrayListChatsModel.add(models);
                        //mChatAdapter.notifyItemInserted(mArrayListChatsModel.size());
                        //sendNotification(models);
                        mChatAdapter.notifyDataSetChanged();
                        recViewChat.scrollToPosition(mChatAdapter.getItemCount() - 1);
                    }


                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.i(TAG, "Group Message Respons" + t.getMessage());
            }
        });
    }

    private void sendNotification(ChatModel models) {

        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);

        builder.addFormDataPart("nameOfReciver", models.getNameOFReciever());
        builder.addFormDataPart("time", models.getTime());
        builder.addFormDataPart("senderId", models.getSenderId());
        builder.addFormDataPart("message", models.getMessage());
        builder.addFormDataPart("senderName", mSessions.getDisplayName());
        builder.addFormDataPart("userImage", mSessions.getImageLink());
        MultipartBody body = builder.build();
        APiInterface aPiInterface = APiClient.createRetrofitService();
        Call<JsonObject> aPiClient = aPiInterface.sendNotification(mUserModel.getUserId(), mSessions.getUSerId(), body, APiClient.API_KEY);
        aPiClient.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() != null) {
                    Log.i(TAG, "response size" + response.body().size());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.i(TAG, "response size" + t.getMessage());
            }
        });
    }


    public void getAllChatGroups() {
        try {
            if (mArrayListChatsModel.size() == 0) {
                showProgressBar();
            }

            APiInterface aPiInterface = APiClient.createRetrofitService();
            Call<JsonObject> userCall = aPiInterface.getAllGroupsChat(mGroupInfoModel.getGroupId(), APiClient.API_KEY);
            userCall.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    try {
                        hideProgressBar();
                        if (response.body() != null && response.message().equals("OK") && response.body().size() != 0) {
                            JsonObject jsonObject = response.body();
                            ArrayList<ChatModel> arrayList = new ArrayList<>();
                            Set<String> set = jsonObject.keySet();
                            ArrayList<String> list = new ArrayList<String>(set);
                            if (list.size() > mArrayListChatsModel.size()) {
                                for (int i = 0; i < jsonObject.size(); i++) {
                                    String id = list.get(i);
                                    JsonObject jsonObject1 = jsonObject.get(id).getAsJsonObject();
                                    if (jsonObject1.size() != 0 && jsonObject1 != null) {
                                        String message = jsonObject1.get("message").getAsString();
                                        String name = jsonObject1.get("nameOfReciver").getAsString();
                                        String time = jsonObject1.get("time").getAsString();
                                        String senderId = jsonObject1.get("senderId").getAsString();
                                        ChatModel models = new ChatModel(message, name, time);
                                        models.setSenderId(senderId);
                                        models.setMessageId(id);
                                        arrayList.add(models);
                                    }
                                }
                                if (mArrayListChatsModel.size() == 0 && arrayList.size() != 0) {
                                    mArrayListChatsModel = arrayList;
                                    mChatAdapter.setData(mArrayListChatsModel);
                                    recViewChat.scrollToPosition(mChatAdapter.getItemCount() - 1);
                                    Log.i(TAG, "ArrayList init" + mArrayListChatsModel.size());
                                    getAllChatGroups();
                                } else if (arrayList.size() > mArrayListChatsModel.size()) {
                                    Log.i(TAG, "Beefore" + mArrayListChatsModel.size());
                                    int insertIndex = mArrayListChatsModel.size();
                                    for (int i = mArrayListChatsModel.size(); i < arrayList.size(); i++) {
                                        {
                                            if (!mArrayListChatsModel.get(i - 1).getTime().equals(arrayList.get(i).getTime())) {
                                                mArrayListChatsModel.add(arrayList.get(i));
                                            }
                                        }
                                        mChatAdapter.notifyItemRangeInserted(insertIndex, mArrayListChatsModel.size());
                                        recViewChat.scrollToPosition(mChatAdapter.getItemCount() - 1);
                                        getAllChatGroups();


                                    }
                                }
                            } else {
                                getAllChatGroups();
                            }
                        }
                    } catch (Exception e) {

                    }
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    getAllChatGroups();
                    hideProgressBar();
                }
            });

        } catch (Exception e) {
        hideProgressBar();
        }
    }
}
