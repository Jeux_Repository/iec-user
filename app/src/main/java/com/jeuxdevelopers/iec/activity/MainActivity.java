package com.jeuxdevelopers.iec.activity;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.JsonObject;
import com.jeuxdevelopers.iec.R;
import com.jeuxdevelopers.iec.adapter.ShowGroupsAdapter;
import com.jeuxdevelopers.iec.adapter.UserListAdapterMain;
import com.jeuxdevelopers.iec.fragment.InboxFragment;
import com.jeuxdevelopers.iec.helper.HelperClass;
import com.jeuxdevelopers.iec.models.GroupInfoModel;
import com.jeuxdevelopers.iec.models.UserModels;
import com.jeuxdevelopers.iec.networks.APiClient;
import com.jeuxdevelopers.iec.networks.APiInterface;
import com.jeuxdevelopers.iec.services.NotificationServices;
import com.jeuxdevelopers.iec.sessions.IecSessions;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, PopupMenu.OnMenuItemClickListener {

    @BindView(R.id.tv_Title)
    TextView tvTitle;
    /* @BindView(R.id.mainHeading)
     RelativeLayout mainHeading;*/
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.appBarLayout)
    AppBarLayout appBarLayout;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.searchView)
    SearchView searchView;
    @BindView(R.id.iv_More)
    ImageView ivMore;
    @BindView(R.id.rclView_Users)
    RecyclerView rclViewUsers;
    @BindView(R.id.tv_SeeMore)
    TextView tvSeeMore;
    @BindView(R.id.lnr_SeeMore)
    LinearLayout lnrSeeMore;
    @BindView(R.id.cv_AllChat)
    CardView cvAllChat;
    @BindView(R.id.rclView_Groups)
    RecyclerView rclViewGroups;
    @BindView(R.id.cv_Groups)
    CardView cvGroups;
    private static String TAG = "MainActivity___";

    @BindView(R.id.nav_view)
    NavigationView navView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.imgView_menu)
    ImageView imgViewMenu;

    private ArrayList<UserModels> mUsersList = new ArrayList<>();
    private UserListAdapterMain mUserListAdapter;
    private IecSessions mSessions;
    private ArrayList<String> mArrayListGroupId = new ArrayList<>();
    private ArrayList<GroupInfoModel> mGroupInfoModelsList = new ArrayList<>();
    private ShowGroupsAdapter mShowGroupsAdapter;

    private Fragment fragment;
    private FragmentTransaction fragmentTransaction;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        init();
        loadAllUsersFromServer();
        settingListener();
        loadUserGroupsFromServer();
        startService();
        checkPermissions();
        enableSearching();
        handlingDrawer();


    }

    private void checkPermissions() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA
                ).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
                if (report.areAllPermissionsGranted()) {
                    Log.i(TAG, "Permission granted");
                } else if (report.isAnyPermissionPermanentlyDenied()) {
                    HelperClass.createToast(MainActivity.this, "Please grant permissions manually in the settings");
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                    intent.setData(uri);
                    startActivity(intent);
                } else {
                    Log.i(TAG, "Permission denied");
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                token.continuePermissionRequest();
            }
        }).check();
    }

    private void loadUserGroupsFromServer() {
        APiInterface aPiInterface = APiClient.createRetrofitService();
        Call<JsonObject> userCall = aPiInterface.getSingleUSerData(mSessions.getUSerId(), APiClient.API_KEY);
        userCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                hideProgressbar();
                if (response.body() != null && response.errorBody() == null) {
                    if (response.code() == 200 || response.code() == 201 || response.message().equals("OK")) {

                        JsonObject jsonObject = response.body();
                        if (jsonObject.has("Groups")) {
                            JsonObject jsonObjectGroups = jsonObject.get("Groups").getAsJsonObject();
                            Set<String> set = jsonObjectGroups.keySet();
                            ArrayList<String> list = new ArrayList<>(set);
                            for (int i = 0; i < list.size(); i++) {
                                mArrayListGroupId.add(jsonObjectGroups.get(list.get(i)).getAsString());
                            }
                            if (mArrayListGroupId.size() != 0) {
                                for (int i = 0; i < mArrayListGroupId.size(); i++) {
                                    getGroupDetail(mArrayListGroupId.get(i));
                                }
                            }

                        }
                    }
                } else if (response.errorBody() != null) {

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                hideProgressbar();

            }
        });
    }

    private void getGroupDetail(String s) {
        showProgressBar();
        APiInterface aPiInterface = APiClient.createRetrofitService();
        Call<JsonObject> userCall = aPiInterface.getGroupDetail(s, APiClient.API_KEY);
        userCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                hideProgressbar();
                if (response.body() != null && response.message().equals("OK") && response.body().size() != 0) {
                    JsonObject jsonObject = response.body();
                    String image = jsonObject.get("Image").getAsString();
                    String displayName = jsonObject.get("displayName").getAsString();
                    String userString = jsonObject.get("users").getAsString();
                    GroupInfoModel model = new GroupInfoModel(image, displayName, userString);
                    model.setGroupId(s);
                    mGroupInfoModelsList.add(model);
                    if (mGroupInfoModelsList.size() != 0) {
                        mShowGroupsAdapter.setData(mGroupInfoModelsList);
                    }

                } else if (response.errorBody() != null) {

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                hideProgressbar();
            }
        });
    }


    private void init() {
        mSessions = new IecSessions(this);
        settingUsersRecyclerView();
        settingGroupRecyclerView();


    }

    private void settingGroupRecyclerView() {
        mShowGroupsAdapter = new ShowGroupsAdapter(this);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mLayoutManager.setStackFromEnd(true);
        rclViewGroups.setLayoutManager(mLayoutManager);
        rclViewGroups.setItemAnimator(new DefaultItemAnimator());
        rclViewGroups.setHasFixedSize(true);
        rclViewGroups.setAdapter(mShowGroupsAdapter);
    }

    private void settingUsersRecyclerView() {
        mUserListAdapter = new UserListAdapterMain(this);
        rclViewUsers.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
        rclViewUsers.setItemAnimator(new DefaultItemAnimator());
        rclViewUsers.setAdapter(mUserListAdapter);
    }

    private void loadAllUsersFromServer() {
        mUsersList.clear();
        APiInterface aPiInterface = APiClient.createRetrofitService();
        Call<JsonObject> userCall = aPiInterface.getAllUsers(APiClient.API_KEY);
        userCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.body() != null && response.errorBody() == null) {
                    JsonObject jsonObject = response.body();
                    Set<String> set = jsonObject.keySet();
                    ArrayList<String> list = new ArrayList<String>(set);
                    for (int i = 0; i < jsonObject.size(); i++) {
                        String id = list.get(i);
                        JsonObject jsonObject1 = jsonObject.get(id).getAsJsonObject();
                        String email = jsonObject1.get("email").getAsString();
                        String name = jsonObject1.get("displayName").getAsString();
                        String imageLink = jsonObject1.get("Image").getAsString();
                        if (!id.equals(mSessions.getUSerId())) {
                            UserModels models = new UserModels(id, imageLink, name, email);
                            mUsersList.add(models);
                        }
                    }
                    if (mUsersList.size() != 0) {
                        mUserListAdapter.setData(mUsersList);
                    }

                } else if (response.errorBody() != null) {

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });

    }

    private void settingListener() {
        ivMore.setOnClickListener(this);
    }

    private void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgressbar() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == ivMore.getId()) {
            showMenu(v);
        }
    }

    private void handlingDrawer() {
       imgViewMenu.setOnClickListener(v-> {
           if (drawerLayout.isDrawerOpen(Gravity.LEFT)) {
               drawerLayout.closeDrawer(Gravity.LEFT);
           } else {
               drawerLayout.openDrawer(Gravity.LEFT);
           }
       });

        TextView textViewCreateGroup = navView.findViewById(R.id.txtView_create_group);
        TextView textViewProfile = navView.findViewById(R.id.txtView_view_profile);
        TextView textViewLogout = navView.findViewById(R.id.txtView_logout);
        ImageView mImageView = navView.findViewById(R.id.imgView_profile);
        TextView txtViewName = navView.findViewById(R.id.txtView_name);
        Glide.with(this).load(mSessions.getImageLink()).into(mImageView);
        txtViewName.setText(mSessions.getDisplayName());
        textViewCreateGroup.setOnClickListener(v -> {
            navigateToCreateGroupActivity();
            drawerLayout.closeDrawer(Gravity.LEFT);
        });
        textViewProfile.setOnClickListener(v -> {
            startActivity(new Intent(MainActivity.this,ProfileActivity.class));
            drawerLayout.closeDrawer(Gravity.LEFT);
        });
        textViewLogout.setOnClickListener( v-> {
            mSessions.setLogin("", "", "", "", false);
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
            drawerLayout.closeDrawer(Gravity.LEFT);
        });

    }

    public void showMenu(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        popup.setOnMenuItemClickListener(this);
        popup.inflate(R.menu.pop_up_menu_main_activity);
        popup.show();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_create_group:
                navigateToCreateGroupActivity();
                return true;
            case R.id.item_logout:

            default:
                return false;
        }
    }

    private void navigateToCreateGroupActivity() {
        if (mUsersList.size() != 0) {
            Intent intent = new Intent(this, CreateGroupActivity.class);
            intent.putExtra(CreateGroupActivity.KEY_USER_LIST, mUsersList);
            startActivity(intent);
        } else {
            HelperClass.createToast(this, "No User Found");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        startService();
        Log.i(TAG, "asfasfasfafasf");
    }

    public void startService() {
        Intent myService = new Intent(MainActivity.this, NotificationServices.class);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(myService);
        } else {
            startService(myService);
        }
    }

    private void enableSearching() {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                Log.v("onQueryTextSubmit", query);
                mUserListAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                Log.v("onQueryTextChange", query);
                // filter recycler view when text is changed
                mUserListAdapter.getFilter().filter(query);
                return false;
            }
        });
    }

    public void showInboxFragment() {
        fragment = new InboxFragment();
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.rl_Container, fragment);
        fragmentTransaction.commit();
    }
}
