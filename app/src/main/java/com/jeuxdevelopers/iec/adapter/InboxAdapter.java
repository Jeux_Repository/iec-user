package com.jeuxdevelopers.iec.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.jeuxdevelopers.iec.R;
import com.jeuxdevelopers.iec.activity.ChatActivity;
import com.jeuxdevelopers.iec.models.UserModels;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;

public class InboxAdapter extends RecyclerView.Adapter<InboxAdapter.InboxViewholder> {

    private Context context;
    private ArrayList<UserModels> data = new ArrayList<>();

    public InboxAdapter(Context context) {
        this.context = context;
    }

    public void setData(ArrayList<UserModels> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public InboxViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_view_inbox, parent, false);
        return new InboxViewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final InboxViewholder holder, int position) {
        final UserModels currentObject = data.get(position);

        String dateString;

        holder.tv_Name.setText(currentObject.getName());
        holder.tv_Message.setText(currentObject.getLastMessage());

        Calendar dateCalender = Calendar.getInstance();
        Calendar current = Calendar.getInstance();

        current.setTimeInMillis(System.currentTimeMillis());
        dateCalender.setTimeInMillis(currentObject.getTimeInMills());

        int currentDate = current.get(Calendar.DATE);
        int currentMonth = current.get(Calendar.DATE);
        int currentYear = current.get(Calendar.DATE);

        int date = dateCalender.get(Calendar.DATE);
        int month = dateCalender.get(Calendar.MONTH);
        int year = dateCalender.get(Calendar.YEAR);

        if (currentDate == date) {
            int hour = dateCalender.get(Calendar.HOUR_OF_DAY);
            int minute = dateCalender.get(Calendar.MINUTE);
            String format = "";
            if (hour == 0) {
                hour += 12;
                format = "AM";
            } else if (hour == 12) {
                format = "PM";
            } else if (hour > 12) {
                hour -= 12;
                format = "PM";
            } else {
                format = "AM";
            }

            String hou = "";
            if (hour < 10) {
                hou = "0" + String.valueOf(hour);
            } else {
                hou = String.valueOf(hour);
            }

            String min = "";
            if (minute < 10) {
                min = "0" + minute;
            } else {
                min = String.valueOf(minute);
            }
            dateString = hou + ":" + min + " " + format;
        } else if (currentMonth == month || currentYear == year) {
            dateString = date + " " + getMonthForInt(month);
        } else {
            dateString = date + " " + getMonthForInt(month) + " " + year;
        }

        holder.tv_Time.setText(dateString);

        Glide.with(context).load(currentObject.getImage()).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {

                holder.civ_Picture.setImageResource(R.drawable.ic_person_black_24dp);
                holder.progressBar.setVisibility(View.GONE);
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                holder.progressBar.setVisibility(View.GONE);
                return false;
            }
        }).into(holder.civ_Picture);


        holder.rl_Parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v("CheckImageMessage", currentObject.getLastMessage());
                context.startActivity(new Intent(context, ChatActivity.class)
                        .putExtra("DATA", currentObject)
                );
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class InboxViewholder extends RecyclerView.ViewHolder {

        private CircleImageView civ_Picture;
        private ProgressBar progressBar;
        private TextView tv_Name, tv_Message, tv_Date, tv_Time;
        private RelativeLayout rl_Parent;

        public InboxViewholder(@NonNull View itemView) {
            super(itemView);
            civ_Picture = itemView.findViewById(R.id.civ_Picture);
            progressBar = itemView.findViewById(R.id.progressBar);
            tv_Name = itemView.findViewById(R.id.tv_Name);
            tv_Message = itemView.findViewById(R.id.tv_Message);
            tv_Date = itemView.findViewById(R.id.tv_Date);
            tv_Time = itemView.findViewById(R.id.tv_Time);
            rl_Parent = itemView.findViewById(R.id.rl_Parent);
        }
    }

    String getMonthForInt(int num) {
        String month = "wrong";
        DateFormatSymbols dfs = new DateFormatSymbols();
        String[] months = dfs.getMonths();
        if (num >= 0 && num <= 11) {
            month = months[num];
        }
        return month;
    }
}
