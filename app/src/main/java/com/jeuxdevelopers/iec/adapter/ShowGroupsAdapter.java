package com.jeuxdevelopers.iec.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.jeuxdevelopers.iec.R;
import com.jeuxdevelopers.iec.activity.ChatActivity;
import com.jeuxdevelopers.iec.helper.Constants;
import com.jeuxdevelopers.iec.models.GroupInfoModel;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.ArrayList;

public class ShowGroupsAdapter extends RecyclerView.Adapter<ShowGroupsAdapter.GroupViewHolder> {
    private ArrayList<GroupInfoModel> mArrayList = new ArrayList<>();
    private Context mContext;


    public ShowGroupsAdapter(Context context) {
        mContext = context;
    }

    @NonNull
    @Override
    public GroupViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext)
                .inflate(R.layout.item_groups, parent, false);
        return new GroupViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull GroupViewHolder holder, int position) {
        GroupInfoModel model = mArrayList.get(position);
        Glide.with(mContext).load(model.getImage()).into(holder.mImageView);
        holder.mTextView.setText(model.getName());

    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class GroupViewHolder extends RecyclerView.ViewHolder {
        CircularImageView mImageView;
        TextView mTextView;
        public GroupViewHolder(@NonNull View itemView) {
            super(itemView);
            mImageView = itemView.findViewById(R.id.imgVIew_group);
            mTextView = itemView.findViewById(R.id.txtView_groupName);
            mImageView.setOnClickListener(v-> {
                Intent intent = new Intent(mContext, ChatActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(ChatActivity.KEY_ACTIVITY_TYPE, Constants.GROUPACTIVITY_Type);
                intent.putExtra(ChatActivity.KEY_USER_MODEL,mArrayList.get(getAdapterPosition()));
                mContext.startActivity(intent);
            });
        }
    }

    public void setData(ArrayList<GroupInfoModel> models) {
        mArrayList = models;
        notifyDataSetChanged();
    }

}
