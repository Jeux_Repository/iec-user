package com.jeuxdevelopers.iec.models;

public class GroupAddModel {
    private String mGroupId;
    private String mRandomKey;

    public GroupAddModel(String groupId, String randomKey) {
        mGroupId = groupId;
        mRandomKey = randomKey;
    }


    public String getGroupId() {
        return mGroupId;
    }

    public void setGroupId(String groupId) {
        mGroupId = groupId;
    }

    public String getRandomKey() {
        return mRandomKey;
    }

    public void setRandomKey(String randomKey) {
        mRandomKey = randomKey;
    }
}
