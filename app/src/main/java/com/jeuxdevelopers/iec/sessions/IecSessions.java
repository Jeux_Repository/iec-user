package com.jeuxdevelopers.iec.sessions;

import android.content.Context;
import android.content.SharedPreferences;

public class IecSessions {
    private static String PREF_NAME = "iec_pref";
    private static String KEY_DISPLAY_NAME = "display_name";
    private static String KEY_EMAIL = "email";
    private String KEY_IS_LOGIN = "is_login";
    private String KEY_IMAGE_LINK = "image_link";
    private String KEY_USER_ID = "userId";
    private SharedPreferences mPreferences;
    private SharedPreferences.Editor mEditor;

    public IecSessions(Context context) {
        mPreferences = context.getSharedPreferences(PREF_NAME, 0); // 0 - for private mode
        mEditor = mPreferences.edit();

    }

    public void setLogin(String id, String email, String name,String imageLink,boolean isLogin) {
        mEditor.putString(KEY_USER_ID,id);
        mEditor.putString(KEY_DISPLAY_NAME, name);
        mEditor.putString(KEY_EMAIL, email);
        mEditor.putBoolean(KEY_IS_LOGIN, isLogin);
        mEditor.putString(KEY_IMAGE_LINK,imageLink);
        mEditor.commit();
    }

    public String getImageLink() { return mPreferences.getString(KEY_IMAGE_LINK,"");}
    public String getDisplayName() {
        return mPreferences.getString(KEY_DISPLAY_NAME, "");
    }
    public String getEmail() {
        return mPreferences.getString(KEY_EMAIL, "");
    }
    public boolean isLogin() {
        return mPreferences.getBoolean(KEY_IS_LOGIN, false);
    }
    public String getUSerId() {
        return  mPreferences.getString(KEY_USER_ID,"");
    }

}
